# Air Quality Monitor



## Group 34
Enviro Sensing HAT Concept and Design

## Description
The air we breathe indoors is just as important as the air we breathe when we are outside. This has become evident with the onset of the COVID-19 pandemic in recent years. Therefore, an enviro sensing HAT is designed as an air quality monitoring device, targeted at facilities and homeowners opting to integrate such devices to keep their employees, visitors, and themselves safe and healthy. 

This device will make use of an analog gas sensor and a digital temperature sensor, to determine the quality of the air. For example, decreased air quality could be caused by the presence of air pollutants or as a result of improper ventilation. If any emissions are identified as harmful, an LED will light up, alerting the user so that they are able to take appropriate action. In addition, the typical room temperature ranges from 20°C to 24°C. Thus, the temperature sensor will track the temperature levels and ensure it stays within this range to optimise comfort and energy efficiency. If it is below or above the typical range, an LED will light up, allowing the user to adjust the thermostat settings or use humidifiers/dehumidifiers. The temperature sensor is typically accurate to ±0.5 °C (typ) –40 °C to +125 °C. 

The PCB currently costs $70 for 5 boards, with a weight of 751g, making it an affordable and user-friendly air quality monitoring device. It is designed for low power consumption and can be mounted on a wall for ease of access.

## Installation
This repo can be accessed by all project team members using git push and git pull. Teaching staff, as well as others interested in this project, will be able to clone this repo to view the progress. This repo contains files that may need additional software to run i.e. CAD files.

## Usage
This git repo is used for version control and sharing of files between project team members. Additionally, it allows teaching staff, as well as others interested, to keep track of the progress of the project.

## Support
Contact any project team member for help or if there are any issues: KMNDYL001@myuct.ac.za | RMXDAN002@myuct.ac.za | JGDSAY001@myuct.ac.za

## Roadmap
There will consistently be new versions of the files in this repo whilst the project is in the developement stages.

## Contributing
Currently, contributions will only be taken by project team members and UCT teaching staff. 

## Authors and acknowledgment
Dylan Kuming | Dan Rom | Sayitha Jugdaw

## License
Creative Commons.

## Project status
Actively in progress.
